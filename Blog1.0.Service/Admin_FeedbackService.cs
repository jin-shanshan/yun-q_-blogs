﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【留言管理】逻辑层
    /// </summary>
    public class Admin_FeedbackService : Repository
    {
        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(FeedbackModel filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var link = db.IQueryable<Guo_Feedback>();
            var qq = db.IQueryable<Guo_QQUser>();
            var query = (from a in link
                         join b in qq on new { SendId = (int)a.SendId } equals new { SendId = b.Id } into b_join
                         from b in b_join.DefaultIfEmpty()
                         join c in qq on new { AcceptId = (int)a.AcceptId } equals new { AcceptId = c.Id } into c_join
                         from c in c_join.DefaultIfEmpty()
                         orderby
                           a.CreateOn descending
                         select new
                         {
                             a.Id,
                             a.SendId,
                             a.AcceptId,
                             a.Content,
                             a.ParentId,
                             a.City,
                             a.Equip,
                             a.CreateOn,
                             SendNickName = b.NickName,
                             AcceptNickName = c.NickName
                         }).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = link.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<FeedbackModel> ReadModel(int Id)
        {
            var db = this.Base();
            var link = db.IQueryable<Guo_Feedback>();
            var qq = db.IQueryable<Guo_QQUser>();
            var query = from guo_feedback in link
                        where guo_feedback.Id == Id
                        select new FeedbackModel()
                        {
                            Id = guo_feedback.Id,
                            SendId = guo_feedback.SendId,
                            AcceptId = guo_feedback.AcceptId,
                            Content = guo_feedback.Content,
                            ParentId = guo_feedback.ParentId,
                            City = guo_feedback.City,
                            Equip = guo_feedback.Equip,
                            CreateOn = guo_feedback.CreateOn,
                            SendNickName =
                            ((from guo_qquser in qq
                              where guo_qquser.Id ==
      ((from guo_feedback0 in link
        where guo_feedback0.Id == Id
        select new
        {
            guo_feedback0.SendId
        }
      ).FirstOrDefault().SendId)
                              select new
                              {
                                  guo_qquser.NickName
                              }
                            ).FirstOrDefault().NickName),
                            AcceptNickName =
                            ((from guo_qquser in qq
                              where guo_qquser.Id ==
                           ((from guo_feedback0 in link
                             where guo_feedback0.Id == Id
                             select new
                             {
                                 guo_feedback0.AcceptId
                             }
                            ).FirstOrDefault().AcceptId)
                              select new
                              {
                                  guo_qquser.NickName
                              }
                            ).FirstOrDefault().NickName)
                        };

            return query.ToList();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_Feedback>(t => t.Id == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_Feedback>(t => t.Id == entity.Id);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }

        /// <summary>
        /// 返回当天留言数量
        /// </summary>
        /// <returns></returns>
        public int GetFeedbackNum()
        {
            var db = this.Base();
            var Feedback = db.IQueryable<Guo_Feedback>();
            var query = from i in Feedback                       
                        select i;
            var timeNow = DateTime.Now.ToString("yyyy-MM-dd");
            DateTime time1 = Convert.ToDateTime(timeNow + " 0:00:00");  //数字前记得加空格
            DateTime time2 = Convert.ToDateTime(timeNow + " 23:59:59");

            query = query.Where(t => t.CreateOn > time1 && t.CreateOn < time2);
            return query.Count();
        }
    }
}
