﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【评论管理】逻辑层
    /// </summary>
    public class Admin_CommentService : Repository
    {
        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(Guo_Comment filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var diary = db.IQueryable<Guo_Comment>();
            var qquser = db.IQueryable<Guo_QQUser>();
            var article = db.IQueryable<Guo_Acticle>();
            var query = (from a in diary
                         join b in qquser on new { SendId = (int)a.SendId } equals new { SendId = b.Id } into b_join
                         from b in b_join.DefaultIfEmpty()
                         join c in qquser on new { AcceptId = (int)a.AcceptId } equals new { AcceptId = c.Id } into c_join
                         from c in c_join.DefaultIfEmpty()
                         join d in article on new { ArticleId = (int)a.ArticleId } equals new { ArticleId = d.ArticleID } into d_join
                         from d in d_join.DefaultIfEmpty()
                         orderby
                           a.CreateOn descending
                         select new
                         {
                             a.Id,
                             a.SendId,
                             a.AcceptId,
                             a.Content,
                             a.Status,
                             a.ParentId,
                             a.ArticleId,
                             a.CreateOn,
                             SendNickName = b.NickName,
                             AcceptNickName = c.NickName,
                             ArticleTitle = d.ArticleTitle
                         }).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = diary.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<CommentModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_Comment>().Where(t => t.Id == Id);
            var qquser = db.IQueryable<Guo_QQUser>();
            var article = db.IQueryable<Guo_Acticle>();
            var query = from i in data
                        join b in qquser on new { SendId = (int)i.SendId } equals new { SendId = b.Id } into b_join
                        from b in b_join.DefaultIfEmpty()
                        join c in qquser on new { AcceptId = (int)i.AcceptId } equals new { AcceptId = c.Id } into c_join
                        from c in c_join.DefaultIfEmpty()
                        join d in article on new { ArticleId = (int)i.ArticleId } equals new { ArticleId = d.ArticleID } into d_join
                        from d in d_join.DefaultIfEmpty()
                        select new CommentModel()
                        {
                            Id = i.Id,
                            SendId = i.SendId,
                            SendNickName = b.NickName,
                            AcceptId = i.AcceptId,
                            AcceptNickName = c.NickName,
                            Content = i.Content,
                            Status = i.Status,
                            ParentId =i.ParentId,
                            ArticleTitle = d.ArticleTitle,
                            CreateOn = i.CreateOn,
                        };

            return query.ToList();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_Comment>(t => t.Id == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_Comment>(t => t.Id == entity.Id);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                throw new Exception("删除失败！");
            }
        }
    }
}
