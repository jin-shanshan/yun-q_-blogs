﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【资源】逻辑层
    /// </summary>
    public class ResourcesService : Repository
    {
        /// <summary>
        /// 返回总数量
        /// </summary>
        /// <returns></returns>
        public int GetDemoNum()
        {
            return this.Base().IQueryable<Guo_Demo>(t => t.IsActive == true).Count();
        }

        /// <summary>
        /// 获取 资源分类
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuInfo> GetTypeList()
        {
            var db = this.Base();
            var type = db.IQueryable<Guo_Resources>();
            var query = from u in type
                        orderby new { u.CRT_Time, u.OrderNo } descending
                        select new MenuInfo
                        {
                            id = u.Id,
                            name = u.Name,
                            pId = u.UserID
                        };
            return query;
        }

        /// <summary>
        /// 资源页面-线性查询
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public string LoadResourceByClass(int classId, int page, int pagesize)
        {
            var db = this.Base();
            var article = db.IQueryable<Guo_Demo>();
            var type = db.IQueryable<Guo_Resources>();
            var catalog = db.IQueryable<Guo_DemoType>();

            #region Linq 分页
            if (classId != 0)
            {
                var query = (from a in article
                             join b in type on new { TypedmId = (int)a.TypedmId } equals new { TypedmId = b.Id }
                             join c in catalog on new { ResourId = (int)a.ResourId } equals new { ResourId = c.Id } into d_join
                             from c in d_join.DefaultIfEmpty()
                             where
                               a.IsActive == true && a.ResourId == classId
                             group new { a, c, b } by new
                             {
                                 a.Id,
                                 a.Title,
                                 a.Abstract,
                                 a.ImgUrl,
                                 DemoTypeName = c.Name,
                                 ResourcesName = b.Name,
                                 a.IsTop,
                                 a.ViewTimes,
                                 a.IsActive,
                                 a.CRT_Time
                             } into g
                             orderby
                               g.Key.IsTop descending,
                               g.Key.CRT_Time descending
                             select new ArticleModel()
                             {
                                 ArticleID = g.Key.Id,
                                 ArticleTitle = g.Key.Title,
                                 Abstract = g.Key.Abstract,
                                 ImgUrl = g.Key.ImgUrl,
                                 CatalogName = g.Key.DemoTypeName,
                                 TypeName = g.Key.ResourcesName,
                                 IsTop = g.Key.IsTop,
                                 ViewTimes = g.Key.ViewTimes,
                                 Replies = g.Count(),
                                 IsActive = g.Key.IsActive,
                                 CRT_Time = g.Key.CRT_Time
                             }).Skip((page - 1) * pagesize).Take(pagesize);

                //页码Page初始值定义为0时则： Skip(page * pagesize).Take(pagesize).ToList();

                string articleHtml = CreateArticleHtml(query);
                return articleHtml;
            }
            else
            {
                var query = (from a in article
                             join b in type on new { TypedmId = (int)a.TypedmId } equals new { TypedmId = b.Id }
                             join c in catalog on new { ResourId = (int)a.ResourId } equals new { ResourId = c.Id } into d_join
                             from c in d_join.DefaultIfEmpty()
                             where
                               a.IsActive == true 
                             group new { a, c, b } by new
                             {
                                 a.Id,
                                 a.Title,
                                 a.Abstract,
                                 a.ImgUrl,
                                 DemoTypeName = c.Name,
                                 ResourcesName = b.Name,
                                 a.IsTop,
                                 a.ViewTimes,
                                 a.IsActive,
                                 a.CRT_Time
                             } into g
                             orderby
                               g.Key.IsTop descending,
                               g.Key.CRT_Time descending
                             select new ArticleModel()
                             {
                                 ArticleID = g.Key.Id,
                                 ArticleTitle = g.Key.Title,
                                 Abstract = g.Key.Abstract,
                                 ImgUrl = g.Key.ImgUrl,
                                 CatalogName = g.Key.DemoTypeName,
                                 TypeName = g.Key.ResourcesName,
                                 IsTop = g.Key.IsTop,
                                 ViewTimes = g.Key.ViewTimes,
                                 Replies = g.Count(),
                                 IsActive = g.Key.IsActive,
                                 CRT_Time = g.Key.CRT_Time
                             }).Skip((page - 1) * pagesize).Take(pagesize);

                string articleHtml = CreateArticleHtml(query);
                return articleHtml;
            }
            #endregion

        }

        /// <summary>
        /// 拼接文章字符串
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private string CreateArticleHtml(IEnumerable<ArticleModel> list)
        {
            StringBuilder sb = new StringBuilder();
            if (list != null)
            {
                foreach (ArticleModel item in list)
                {
                    string links = "/Resources/Detail?Id=" + item.ArticleID;
                    #region 拼接字符串
                    sb.AppendFormat(@"<div class='col-lg-1-5 col-6 col-sm-6 col-md-4 col-lg-3'>
                                      <article id='post-2110' class='post post-grid post-2110 type-post status-publish format-standard has-post-thumbnail hentry category-ym tag-56'>
                                        <div class='entry-media'>
                                             <div class='placeholder' style='padding-bottom: 66.666666666667%;'>
                                             <a target='blank' href='{0}'> 
                                            <img src='{1}' alt='ZQ个人博客模板' />   
                                            <div class='cao-cover'>
                                            <img src='https://www.3qym.com/wp-content/themes/ripro/assets/images/svg/rings.svg' width='50' height='50px' />
                                                </div>
                                                </a></div> </div>
                                             <div class='entry-wrapper'>
                                                 <header class='entry-header'>
                                                <div class='entry-meta'>
                                                    <span class='meta-category'>
                                                        <a rel = 'category' > <i class='dot'></i>{2}</a>
                                                    </span>
                                                </div>
                                                <h2 class='entry-title'><a target = '_blank' href='{3}' title='{4}' rel='bookmark'>{5}</a></h2>
                                            </header>
                                            <div class='entry-footer'>
                                                <ul class='post-meta-box'>
                                                    <li class='meta-date'> <time datetime = '{6}'><i class='fa fa-clock-o'></i> {7}</time> </li>
                                                    <li class='meta-views'>
                                                        <span>
                                                            <i class='fa fa-eye'></i>
                                                              {8}
                                                        </span>
                                                    </li>
                                                    <li class='meta-comment'><span><i class='fa fa-comments-o'></i>{9}</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </article></div>", links,item.ImgUrl,item.TypeName,
                                    links,item.ArticleTitle,item.ArticleTitle,item.CRT_Time,item.CRT_Time,item.ViewTimes,item.Replies
                                   );
                    #endregion
                }
            }
            return sb.ToString();
        }
    }
}
