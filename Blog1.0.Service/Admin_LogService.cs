﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【日志管理】逻辑层
    /// </summary>
    public class Admin_LogService : Repository
    {
        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(LogModel filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var link = db.IQueryable<Guo_SysLog>();
            var user = db.IQueryable<Guo_User>();
            var query = (from i in link
                         join t in user on
                         i.OP_Eid equals t.ID
                         orderby i.Time descending
                         select new
                         {
                             Id = i.RowId,
                             UserName = t.LoginID,
                             RealName = i.OP_Ename,
                             Description = i.Describe,
                             IPAddress = i.Ip,
                             IPAddressName = i.IpAddressName,
                             Status = i.Results,
                             CreateOn = i.Time

                         }).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = link.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_SysLog>(t => t.RowId == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_SysLog>(t => t.RowId == entity.RowId);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteByWhere(IEnumerable<LogModel> list)
        {
            var db = this.Base();
            try
            {
                foreach (var item in list)
                {
                    db.Delete<Guo_SysLog>(t => t.RowId == item.Id);
                }
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }
    }
}
