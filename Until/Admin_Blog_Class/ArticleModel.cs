﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// 【文章管理】 返回实体类
    /// </summary>
    public class ArticleModel
    {
        public int ArticleID { get; set; }
        /// <summary>
        /// 文章类型ID
        /// </summary>
        public int TypeID { get; set; }
        /// <summary>
        /// 文章类型名称
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 文章分类ID
        /// </summary>
        public int CatalogID { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CatalogName { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        public string ArticleTitle { get; set; }
        /// <summary>
        /// 文章封面
        /// </summary>
        public string ImgUrl { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        public string Abstract { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime CRT_Time { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public Nullable<System.DateTime> Up_Time { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public Nullable<int> UserID { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 点击率（阅读量）
        /// </summary>
        public Nullable<int> ViewTimes { get; set; }
        /// <summary>
        /// 评论数
        /// </summary>
        public Nullable<int> Replies { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        public Nullable<bool> IsTop { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Years { get; set; }
        public string Test { get; set; }
        /// <summary>
        /// 网易云外链
        /// </summary>
        public string Wyyun { get; set; }
    }
}
