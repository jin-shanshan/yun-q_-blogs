﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    /// <summary>
    /// 文章分类-绑定类
    /// </summary>
    public class ArticleClassModel
    {
        public int CatalogID { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CatalogName { get; set; }        
        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public Nullable<int> OrderNo { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public Nullable<bool> IsActive { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CRT_Time { get; set; }
    }
}
