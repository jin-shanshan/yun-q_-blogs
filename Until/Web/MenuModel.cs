﻿using System;

namespace Until
{
    /// <summary>
    /// 后台管理菜单类
    /// </summary>
    public class MenuModel
    {
        public int Id { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// 菜单路径
        /// </summary>
        public string MenuUrl { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string MenuIcon { get; set; }
        /// <summary>
        /// 父菜单
        /// </summary>
        public int? ParentId { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        public int? OrderNo { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public int? CreateBy { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public bool? Status { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateOn { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public int? UpdateBy { get; set; }
        /// <summary>
        /// 菜单操作字符串
        /// </summary>
        public string MenuActionHtml { get; set; }
        public bool IsChecked { get; set; }
    }
}
