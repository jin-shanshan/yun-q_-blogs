﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    /// <summary>
    /// 后台管理 用户类
    /// </summary>
    public partial class Guo_User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int ID { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string LoginID { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public Nullable<int> RoleId { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string PassWord { get; set; }

        /// <summary>
        /// 账户状态
        /// </summary>
        public Nullable<bool> Status { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<DateTime> CreateOn { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public Nullable<DateTime> UpdateOn { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public Nullable<int> CreateBy { get; set; }

        /// <summary>
        /// 修改者
        /// </summary>
        public Nullable<int> UpdateBy { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Nullable<int> Gender { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string HeadShot { get; set; }
    }
}
