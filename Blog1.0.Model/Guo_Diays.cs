﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    /// <summary>
    /// 日记类
    /// </summary>
    public partial class Guo_Diays
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]//代表ID自增
        public int Id { get; set; }
        /// <summary>
        /// 日记内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime CreateOn { get; set; }
    }
}
