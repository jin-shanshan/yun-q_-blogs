﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog1._0.Model
{
    /// <summary>
    /// QQ用户 类
    /// </summary>
    public partial class Guo_QQUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        /// <summary>
        /// qq用户标识
        /// </summary>
        public string OpenId { get; set; }
        /// <summary>
        /// qq昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int Gender { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadShot { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<bool> Status { get; set; }
        /// <summary>
        /// 最后一次登录时间
        /// </summary>
        public Nullable<System.DateTime> LastLogin { get; set; }
        /// <summary>>
        /// 创建日期
        /// </summary>
        public Nullable<System.DateTime> CreateOn { get; set; }
    }
}
