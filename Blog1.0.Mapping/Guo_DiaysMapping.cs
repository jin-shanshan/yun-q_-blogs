﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_DiaysMapping : EntityTypeConfiguration<Guo_Diays>
    {
        public Guo_DiaysMapping()
        {
            this.ToTable("Guo_Diays");
            this.HasKey(t => t.Id);
        }
    }
}
