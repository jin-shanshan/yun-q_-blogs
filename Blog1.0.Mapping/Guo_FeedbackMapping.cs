﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_FeedbackMapping : EntityTypeConfiguration<Guo_Feedback>
    {
        public Guo_FeedbackMapping()
        {
            this.ToTable("Guo_Feedback");
            this.HasKey(t => t.Id);
        }
    }
}
