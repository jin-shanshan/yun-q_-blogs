﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_Admin_Role_MenuMapping : EntityTypeConfiguration<Guo_Admin_Role_Menu>
    {
        public Guo_Admin_Role_MenuMapping()
        {
            this.ToTable("Guo_Admin_Role_Menu");
            this.HasKey(t=>t.RowId);
        }
    }
}
