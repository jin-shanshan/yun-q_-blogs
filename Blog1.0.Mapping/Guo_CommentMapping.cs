﻿using Blog1._0.Model;
using System.Data.Entity.ModelConfiguration;

namespace Blog1._0.Mapping
{
    public class Guo_CommentMapping : EntityTypeConfiguration<Guo_Comment>
    {
        public Guo_CommentMapping()
        {
            this.ToTable("Guo_Comment");
            this.HasKey(t => t.Id);
        }
    }
}
