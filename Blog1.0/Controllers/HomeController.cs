﻿using Blog1._0.Service;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【首页】控制器
    /// </summary>
    public class HomeController : Controller
    {
        WebSiteInfo model = new WebSiteInfo();

        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns> 
        public ActionResult Index()
        {
            ArticleService article = new ArticleService();
            ViewBag.HotArtileList = article.GetHotArticle(3);//热门文章
            return View(model.GetWebSiteInfo());
        }

        /// <summary>
        /// META标签描述 引用
        /// </summary>
        /// <returns></returns>
        public ActionResult Meta()
        {
            ViewBag.Site = model.GetWebSiteInfo();
            return PartialView("~/Views/Shared/_Meta.cshtml");
        }

        /// <summary>
        /// 【博客】头部 母版视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Head()
        {
            ViewBag.Site = model.GetWebSiteInfo();
            return PartialView("~/Views/Shared/_Head.cshtml");
        }

        /// <summary>
        /// 【博客】尾部 母版视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Foot()
        {
            ViewBag.Site = model.GetWebSiteInfo();
            return PartialView("/Views/Shared/_Foot.cshtml");
        }
    }
}