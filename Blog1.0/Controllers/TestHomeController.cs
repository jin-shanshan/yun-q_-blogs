﻿using Blog1._0.Cache;
using Blog1._0.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Until;
using Newtonsoft.Json;
namespace Blog1._0.Controllers
{
    public class TestHomeController : Controller
    {
        HomeService home = new HomeService();
        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            HomeService home = new HomeService();
            MenuCache cache = new MenuCache();
            var list = cache.GetListMenuInfo();
            var list1 = JsonConvert.DeserializeObject(home.GetListLabelInfo());
            ViewBag.list1 = list1;
            ViewBag.list = list;
            return View();
        }

        /// <summary>
        /// 留言簿 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Message()
        {
            MenuCache cache = new MenuCache();
            var list = cache.GetListMenuInfo();
            ViewBag.list = list;
            return View();
        }

        /// <summary>
        /// 日记 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Diary()
        {
            MenuCache cache = new MenuCache();
            var list = cache.GetListMenuInfo();
            ViewBag.list = list;
            return View();
        }

        /// <summary>
        /// 相册 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Photo()
        {
            MenuCache cache = new MenuCache();
            var list = cache.GetListMenuInfo();
            ViewBag.list = list;
            return View();
        }

        /// <summary>
        /// 友情链接 视图
        /// </summary>
        /// <returns></returns>
        public ActionResult FriendUrl()
        {
            MenuCache cache = new MenuCache();
            var list = cache.GetListMenuInfo();
            ViewBag.list = list;
            return View();
        }

        /// <summary>
        /// 文章 详情视图
        /// </summary>
        /// <returns></returns>
        public ActionResult ReadInfo(string Id, string Title)
        {
            MenuCache cache = new MenuCache();
            var list = cache.GetListMenuInfo();
            ViewBag.list = list;
            ViewBag.id = Convert.ToInt16(Id);
            ViewBag.title = Title;
            return View("ReadInfo");
        }

        public ActionResult Test()
        {
            MenuCache cache = new MenuCache();
            HomeService home = new HomeService();
            var list = cache.GetListMenuInfo();
            var list1 = JsonConvert.DeserializeObject(home.GetListLabelInfo());
            ViewBag.list = list;
            ViewBag.list1 = list1;
            return View();
        }

        /// <summary>
        /// 文章加载
        /// </summary>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public ActionResult GetDepotList(int PageNum, int PageSize)
        {
            var list = home.GetListJson(PageNum, PageSize);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 指定文章详情
        /// </summary>
        /// <param name="ArticleId"></param>
        /// <returns></returns>
        public ActionResult GetArticleList(int ArticleId)
        {
            var list = home.GetArticleList(ArticleId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取 文章标题
        /// </summary>
        /// <param name="ArticleId"></param>
        /// <returns></returns>
        public ActionResult GetTitle(int ArticleId)
        {
            var list = home.GetTitle(ArticleId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 插入 文章点击率
        /// </summary>
        /// <param name="ArticleId"></param>
        /// <returns></returns>
        public ActionResult GetInsertView(int ArticleId)
        {
            var list = home.GetInsertView(ArticleId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}