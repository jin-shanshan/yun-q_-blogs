﻿using Blog1._0.Service;
using System.Collections.Generic;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【友链】控制器
    /// </summary>
    public class LinksController : Controller
    {
        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            LinksService service = new LinksService();
            WebSiteInfo siteInfo = new WebSiteInfo();
            ViewBag.SiteTitle = siteInfo.GetWebSiteInfo().SiteTitle;
            IEnumerable<LinksModel> LinksList = service.GetAll();
            return View(LinksList);
        }

    }
}